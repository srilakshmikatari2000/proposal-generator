import React from 'react';
import {Switch , Route , Redirect} from 'react-router-dom';
import Login from './components/login';
import NavbarClient from './components/NavbarClient';
//import UserManagement from './components/userManagement';

function App() {
  return (
    <div>
      <Switch>
        <Route exact path="/">
          <Redirect exact from="/" to="/login" />
        </Route>
        <Route path="/login">
          <Login />
        </Route>
        <Route path="/client/navbar">
          <NavbarClient />
        </Route>
      </Switch>
    </div>
  )
}

export default App
