import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles({
    outerH: {
        width: '350px',
        height: '100vh',
        background: '#263238',
        color: '#FFF',

        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',

        position: 'fixed',
        top: '0',
        left: '0',
    },
    outer: {
        width: '100px',
        height: '100vh',
        background: '#263238',
        color: '#FFF',

        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',

        position: 'fixed',
        top: '0',
        left: '0',
    },
    logo: {
        width: '100%',
        height: '20vh',

        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    logoHandle: {
        width: '60px',
        height: '60px',
        background: 'rgba(255,255,255,0.5)',
        borderRadius: '50%',
    },
    list: {
        width: '100%',
        height: '80vh',
        overflow: 'auto',

        display: 'grid',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    logoTextH: {
        display: 'none',
    },
    listItem: {
        width: '50px',
        height: '50px',
        margin: '.5rem',
        background: '#151E23',
        borderRadius: '10px',

        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    listItemH: {
        width: '90%',
        padding: '1rem',
        margin: '.5rem',
        background: '#151E23',
        borderRadius: '10px',

        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    listItemActiveH: {
        width: '90%',
        padding: '1rem',
        margin: '.5rem',
        border: '2px solid rgba(255,255,255,1)',
        borderRadius: '10px',

        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    listItemActive: {
        width: '50px',
        height: '50px',
        margin: '.5rem',
        border: '2px solid rgba(255,255,255,1)',
        borderRadius: '10px',

        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    iconH: {
        color: 'rgba(224,224,224,70%)',
        width: '20%',
    },
    icon: {
        color: 'rgba(224,224,224,70%)',
        width: '20%',
        transform: 'scale(2)',
    },
    iconActiveH: {
        color: 'rgba(255,255,255,1)',
        width: '20%',
    },
    iconActive: {
        color: 'rgba(255,255,255,1)',
        width: '20%',
        transform: 'scale(2)',
    },
    linkText: {
        display: 'none',
    },
    linkTextH: {
        width: '80%',
        fontSize: '1.2rem',
        color: 'rgba(224,224,224,70%)',
    },
    linkTextActive: {
        display: 'none',
    },
    linkTextActiveH: {
        width: '80%',
        fontSize: '1.2rem',
        color: 'rgba(255,255,255,1)',
    },
})