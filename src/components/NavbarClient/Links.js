import React from 'react';
import {Link, Typography} from '@material-ui/core';
import {useStyles} from './styles';

import PeopleAltRoundedIcon from '@material-ui/icons/PeopleAltRounded';
import FlipToFrontRoundedIcon from '@material-ui/icons/FlipToFrontRounded';
import ShoppingCartRoundedIcon from '@material-ui/icons/ShoppingCartRounded';
import ComputerRoundedIcon from '@material-ui/icons/ComputerRounded';
import HeadsetMicRoundedIcon from '@material-ui/icons/HeadsetMicRounded';
import ContactPhoneRoundedIcon from '@material-ui/icons/ContactPhoneRounded';
import ViewDayRoundedIcon from '@material-ui/icons/ViewDayRounded';
import CategoryRoundedIcon from '@material-ui/icons/CategoryRounded';

function Links({isHover}) {

    const styles = useStyles();

    return (
        <>
            <Link
                href="/login"
                className={
                    isHover
                    ? styles.listItemActiveH
                    : styles.listItemActive
                }
            >
                <PeopleAltRoundedIcon 
                    className={
                        isHover
                        ? styles.iconActiveH
                        : styles.iconActive
                    }/>
                <Typography 
                    variant={'h6'} 
                    className={
                        isHover
                        ? styles.linkTextActiveH
                        : styles.linkTextActive
                    }>User Management</Typography>
            </Link>
            <Link
                href="/login"
                className={
                    isHover
                    ? styles.listItemH
                    : styles.listItem
                }
            >
                <FlipToFrontRoundedIcon 
                    className={
                        isHover
                        ? styles.iconH
                        : styles.icon
                    }
                    />
                <Typography variant={'h6'} className={
                    isHover
                    ? styles.linkTextH
                    : styles.linkText
                }>Migration Options</Typography>
            </Link>
            <Link
                href="/login"
                className={
                    isHover
                    ? styles.listItemH
                    : styles.listItem
                }
            >
                <ShoppingCartRoundedIcon 
                    className={
                        isHover
                        ? styles.iconH
                        : styles.icon
                    }
                    />
                <Typography variant={'h6'} className={
                    isHover
                    ? styles.linkTextH
                    : styles.linkText
                }>Product List</Typography>
            </Link>
            <Link
                href="/login"
                className={
                    isHover
                    ? styles.listItemH
                    : styles.listItem
                }
            >
                <ComputerRoundedIcon 
                    className={
                        isHover
                        ? styles.iconH
                        : styles.icon
                    }
                    />
                <Typography variant={'h6'} className={
                    isHover
                    ? styles.linkTextH
                    : styles.linkText
                }>Software Management</Typography>
            </Link>
            <Link
                href="/login"
                className={
                    isHover
                    ? styles.listItemH
                    : styles.listItem
                }
            >
                <HeadsetMicRoundedIcon 
                    className={
                        isHover
                        ? styles.iconH
                        : styles.icon
                    }
                    />
                <Typography variant={'h6'} className={
                    isHover
                    ? styles.linkTextH
                    : styles.linkText
                }>Quick Start Service</Typography>
            </Link>
            <Link
                href="/login"
                className={
                    isHover
                    ? styles.listItemH
                    : styles.listItem
                }
            >
                <ContactPhoneRoundedIcon 
                    className={
                        isHover
                        ? styles.iconH
                        : styles.icon
                    }
                    />
                <Typography variant={'h6'} className={
                    isHover
                    ? styles.linkTextH
                    : styles.linkText
                }>Contact Details</Typography>
            </Link>
            <Link
                href="/login"
                className={
                    isHover
                    ? styles.listItemH
                    : styles.listItem
                }
            >
                <ViewDayRoundedIcon 
                    className={
                        isHover
                        ? styles.iconH
                        : styles.icon
                    }
                    />
                <Typography variant={'h6'} className={
                    isHover
                    ? styles.linkTextH
                    : styles.linkText
                }>Day Conversion</Typography>
            </Link>
            <Link
                href="/login"
                className={
                    isHover
                    ? styles.listItemH
                    : styles.listItem
                }
            >
                <CategoryRoundedIcon 
                    className={
                        isHover
                        ? styles.iconH
                        : styles.icon
                    }
                    />
                <Typography variant={'h6'} className={
                    isHover
                    ? styles.linkTextH
                    : styles.linkText
                }>GST Percentage</Typography>
            </Link>
            <Link
                href="/login"
                className={
                    isHover
                    ? styles.listItemH
                    : styles.listItem
                }
            >
                <PeopleAltRoundedIcon 
                    className={
                        isHover
                        ? styles.iconH
                        : styles.icon
                    }
                    />
                <Typography variant={'h6'} className={
                    isHover
                    ? styles.linkTextH
                    : styles.linkText
                }>Product List</Typography>
            </Link>
        </>
    )
}

export default Links
