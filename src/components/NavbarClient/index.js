import React,{useState} from 'react';
import {Box, Typography} from '@material-ui/core';
import {useStyles} from './styles';
import Links from './Links';

function NavbarClient() {

    const [isHover,setisHover] = useState(false)

    const styles = useStyles();

    return (
        <Box 
            className={
                isHover
                ? styles.outerH
                : styles.outer
            }
            onMouseOver={()=>setisHover(true)}
            onMouseLeave={()=>setisHover(false)}>
            <Box className={styles.logo}>
                <Box className={styles.logoHandle}></Box>
                <Typography 
                    variant={'h3'}
                    className={
                        isHover
                        ? ''
                        : styles.logoTextH
                    }>LexisNexis</Typography>
            </Box>
            <Box className={styles.list}>
                <Links isHover={isHover}/>
            </Box>
        </Box>
    )
}

export default NavbarClient
