import React,{useEffect} from 'react';
import { Box,FormControl,FormControlLabel,Checkbox,Button,Link, Typography} from '@material-ui/core';
import {Grid,InputBase,InputAdornment,IconButton} from '@material-ui/core';
import PersonOutlineOutlinedIcon from '@material-ui/icons/PersonOutlineOutlined';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import useStyles from './styles.js';

function Login() {

    const [values, setValues] = React.useState({
        mail: '',
        password: '',
        showPassword: false,
        rememberMe: false,
        isMailCorrect: true,
        isPasswordCorrect: true,
        isValidated: false,
    });

    const styles = useStyles();

    const handleChkBox = () => {
        setValues({ ...values, rememberMe: !values.rememberMe });
    };

    const loginAuth = () => {

    }

    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };
    
    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const validatePassword = (key) => {
        if(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}$/.test(key) || key.length <= 0){
            setValues({...values,isPasswordCorrect: true,password: key});
        }
        else{
            setValues({...values,isPasswordCorrect: false,password: key});
        }
    }

    const validateMail = (mail) => {
        if(/[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+/.test(mail) || mail.length <= 0){
            setValues({...values,isMailCorrect: true,email: mail});
        }
        else{
            setValues({...values,isMailCorrect: false,email: mail});
        }
    }

    return (
        <Box className={styles.outer}>
            <Box className={styles.inner}>
                <Box className={styles.logoHolder}>
                    {/*logo comes here*/}
                    <Typography variant={'h6'}>Logo</Typography>
                </Box>
                <Box className={styles.innerinner}>
                    <Typography className={styles.text} variant={'h3'}>Welcome</Typography>
                    <Box className={styles.form}>
                        <FormControl 
                            className={
                                values.isMailCorrect 
                                ? styles.inputBox
                                : styles.inputBoxErr
                            }>
                            <Grid container spacing={3} alignItems="flex-end" className={styles.inputBox2}>
                                <Grid item xs={1}>
                                    <PersonOutlineOutlinedIcon className={styles.icons}/>
                                </Grid>
                                <Grid item xs={11}>
                                    <InputBase 
                                        id="input-mail" 
                                        placeholder="Mail ID" 
                                        inputProps={{ 'aria-label': 'naked' }}
                                        className={styles.input}
                                        onChange={(e) => validateMail(e.target.value)}
                                    />
                                </Grid>
                            </Grid>
                        </FormControl>
                        <FormControl 
                            className={
                                values.isPasswordCorrect 
                                ? styles.inputBox
                                : styles.inputBoxErr
                            }
                        >
                            <Grid container spacing={3} alignItems="flex-end" className={styles.inputBox2}>
                                <Grid item xs={1}>
                                    <LockOutlinedIcon className={styles.icons}/>
                                </Grid>
                                <Grid item xs={11}>
                                    <InputBase
                                        id="input-password"
                                        className={styles.input}
                                        placeholder="Password"
                                        inputProps={{ 'aria-label': 'naked' }}
                                        type={values.showPassword ? 'text' : 'password'}
                                        value={values.password}
                                        onChange={
                                            (e)=>{
                                                validatePassword(e.target.value);
                                            }
                                        }
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={handleClickShowPassword}
                                                    onMouseDown={handleMouseDownPassword}
                                                >
                                                {
                                                values.showPassword ? 
                                                    <Visibility className={styles.icons}/> : 
                                                    <VisibilityOff className={styles.icons}/>
                                                }
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                    />
                                </Grid>
                            </Grid>
                        </FormControl>
                        {
                                values.isPasswordCorrect
                                ? ''
                                : <p className={styles.helpText}>Your password must be greater than 8 in length,must contain atleast 1 capital alphabet and one special charecter,one number.</p>
                        }
                        <FormControl className={styles.chkBox}>
                            <FormControlLabel
                                control={
                                            <Checkbox 
                                                checked={values.rememberMe} 
                                                name="remember me" 
                                                onClick={handleChkBox}
                                                className={styles.checked}
                                            />
                                        }
                                label="Remember me"
                                className={styles.checker}
                            />
                        </FormControl>
                        {
                            values.isValidated
                            ? <Button 
                                variant="contained" 
                                className={styles.Button}
                                onClick={loginAuth}>Login</Button>
                            : <Button 
                                variant="contained" 
                                className={styles.Button}>Login</Button>
                        }
                        <Typography className={styles.formLinks}>
                            <Link href="/forgotpassword">
                                forgot password?
                            </Link>
                            <Link href="/signup">
                                sign up?
                            </Link>
                        </Typography>
                    </Box>
                </Box>
            </Box>
        </Box>
    )
}

export default Login
