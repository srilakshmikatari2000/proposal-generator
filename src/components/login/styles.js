import {makeStyles} from '@material-ui/core';
import bgImg from '../images/bg-img.png';

const useStyles = makeStyles({
    outer : {
        width : '100vw',
        height: '100vh',
        //background: 'linear-gradient(45deg,#7000FF,#FF4B77,#7000FF)',
        background: `url(${bgImg})`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundSize: 'cover',

        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    inner: {
        width: '400px',
        height: '70%',
        maxHeight: '600px',
        background: 'rgba(255,255,255,.15)',
        boxShadow: 'rgba(255, 255, 255, 0.3) 0px 2px 5px, rgba(255, 255, 255, 0.3) 0px 1px 3px;',
        color: '#FFF',
        borderRadius: '20px',
        zIndex: 9,
        /*border: '5px solid transparent',
        borderImage: 'linear-gradient(45deg, rgba(255,255,255,.3), rgba(0, 0, 0, 0))',
        borderImageSlice: 1,*/
        backdropFilter: 'blur(150px)',

        position: 'relative',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-end',
        alignItems: 'center',

        '@media (max-width:500px)': {
            width: '90%',
            height: '100%',
        },
        '@media (max-width:400px)': {
            width: '95%',
            height: '100%',
        },
    },
    innerinner: {
        width: '100%',
        height: '90%',

        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center',

        '@media (max-width:500px)': {
            height: '70%',
        },
        '@media (max-width:400px)': {
            height: '70%',
        },
    },
    form : {
        width: '100%',

        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputBox: {
        width: '80%',
        height: '50px',
        background: 'rgba(255,255,255,.15)',
        borderRadius: '5px',
        margin: '.5rem 0',

        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',

        '@media (max-width:400px)': {
            width: '95%',
        },
    },
    inputBoxErr: {
        width: '80%',
        height: '50px',
        background: 'rgba(255,255,255,.2)',
        borderRadius: '5px',
        border: 'solid 2px red',
        margin: '.5rem 0',

        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',

        '@media (max-width:400px)': {
            width: '95%',
        },
    },
    inputBox2: {
        width: '100%',
        height: '100%',
        border: '0px solid transparent',
        color: '#FFF',

        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    input: {
        color: 'rgba(255,255,255,.7)',
        width: '90%',
    },
    helpText: {
        width: '80%',
        fontSize: '10px',
        color: 'red',

        '@media (max-width:400px)': {
            width: '95%',
        },
    },
    icons: {
        color: 'rgb(255,255,255,0.7)'
    },
    chkBox: {
        width: '80%',

        display: 'flex',
        alignItems: 'flex-start',

        '@media (max-width:400px)': {
            width: '95%',
        },
    },
    checker: {
        transform: 'scale(0.7)',
    },
    checked: {
        color: '#FFF',
        fill: '#FFF'
    },
    Button: {
        width: '40%',
        height: '40px',
        backgroundColor: '#7000FF',
        color: '#FFF',
        float: 'right',
        boxShadow: 'none',

        "&:hover": {
            backgroundColor: '#7000FF',
            boxShadow: 'none',
        },

        '@media (max-width:500px)': {
            width: '50%',
        },
        '@media (max-width:400px)': {
            width: '50%',
        },
    },
    ButtonDis: {
        width: '40%',
        height: '40px',
        backgroundColor: '#c6c6c6',
        color: '#000',
        float: 'right',
        boxShadow: 'none',

        "&:hover": {
            backgroundColor: '#c6c6c6',
            boxShadow: 'none',
        },

        '@media (max-width:500px)': {
            width: '50%',
        },
        '@media (max-width:400px)': {
            width: '50%',
        },
    },
    formLinks: {
        margin: '2rem 0',
        transform: 'scale(0.7)',
    },
    logoHolder: {
        height: '70px',
        width: '120px',
        borderRadius: '100px 100px 0 0',
        background: 'rgba(255,255,255,.15)',
        backdropFilter: 'blur(150px)',

        position: 'absolute',
        top: '0%',
        left: '50%',
        transform: 'translate(-50%,-100%)',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',

        '@media (max-width:500px)': {
            width: '100px',
            height: '100px',
            borderRadius: '50%',
            background: 'rgba(255,255,255,.15)',

            position: 'relative',
            top: '0',
            left: '0',
            transform: 'translate(0,0)',
        },
        '@media (max-width:400px)': {
            width: '80px',
            height: '80px',
            borderRadius: '50%',
            background: 'rgba(255,255,255,.15)',

            position: 'relative',
            top: '0',
            left: '0',
            transform: 'translate(0,0)',
        },
    },
})

export default useStyles;